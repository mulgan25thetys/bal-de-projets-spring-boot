FROM openjdk:8-jdk-alpine
EXPOSE 8083
ADD target/bal-des-projets-1.3.0-SNAPSHOT.jar bal-des-projets-1.3.0-SNAPSHOT.jar
ENTRYPOINT ["java","-jar","/bal-des-projets-1.3.0-SNAPSHOT.jar"]