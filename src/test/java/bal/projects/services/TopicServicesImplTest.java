package bal.projects.services;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertNull;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.junit.Test;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.context.SpringBootTest;

import bal.projects.entities.Topic;
import bal.projects.repositories.TopicRepository;

@ExtendWith(MockitoExtension.class)
@SpringBootTest
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class TopicServicesImplTest {

	private final static Logger log = Logger.getLogger(UserServicesImplTest.class);
	
	@InjectMocks
	TopicServicesImpl topicServices = Mockito.mock(TopicServicesImpl.class);
	
	@Mock
	TopicRepository topicRepository = Mockito.mock(TopicRepository.class);
	
	Topic topic = Topic.builder().dateCreation(new Date()).theme("new theme").build();
	
	@SuppressWarnings("serial")
	List<Topic> list = new ArrayList<Topic>() {
		{
			add(Topic.builder().dateCreation(new Date()).theme("theme2").build());
			add(topic);
		}
	};
	
	@Test
	@Order(0)
	public void testAddTopic() {
		Mockito.when(topicRepository.save(Mockito.any(Topic.class))).then(invocation -> {
			Topic model = invocation.getArgument(1, Topic.class);
			model.setIdTopic((long)1);
			return model;
		});
		
		log.info("========== Avant ==========="+ topic.toString());
		Topic newTopic = topicServices.addTopic(topic);
		assertNotSame(newTopic, topic);
		log.info("========== Après ==========="+ topic.toString());
	}
	
	@Test
	@Order(1)
	public void testEditTopic() {
		Mockito.when(topicRepository.save(Mockito.any(Topic.class))).then(invocation -> {
			Topic model = invocation.getArgument(1, Topic.class);
			model.setIdTopic((long)1);
			return model;
		});
		
		log.info("========== Avant ==========="+ topic.toString());
		Topic newTopic = topicServices.editTopic(topic);
		assertNotSame(newTopic, topic);
		log.info("========== Après ==========="+ topic.toString());
	}
	
	@Test
	@Order(2)
	public void testRemoveTopic() {
		Mockito.when(topicRepository.getById(Mockito.anyLong())).thenReturn(topic);
		log.info(topic.toString());
		topicServices.removeTopic((long)1);
		Topic model = topicRepository.getById((long)1);
		assertNotNull(model);
	}
	
	@Test
	@Order(3)
	public void testGetTopic() {
		Mockito.when(topicRepository.getById(Mockito.anyLong())).thenReturn(topic);
		Topic gottenTopic = topicServices.getTopic((long)2);
		assertNull(gottenTopic);
	}
}
