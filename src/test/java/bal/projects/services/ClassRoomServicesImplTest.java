package bal.projects.services;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertNull;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageRequest;

import bal.projects.entities.ClassRoom;
import bal.projects.repositories.ClassRoomRepository;

@ExtendWith(MockitoExtension.class)
@SpringBootTest
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class ClassRoomServicesImplTest {

	private final static Logger log = Logger.getLogger(UserServicesImplTest.class);
	
	@Autowired
	IClassRoomServices classServices;
	
	@Autowired
	ClassRoomRepository classRepository;
	
	ClassRoom classRoom = ClassRoom.builder().bloc("E").dateCreation(new Date()).build();
	
	@SuppressWarnings("serial")
	List<ClassRoom> list = new ArrayList<ClassRoom>() {
		{
			add(classRoom);
			add(ClassRoom.builder().bloc("B").dateCreation(new Date()).build());
		}
	};
	
	@Test
	@Order(0)
	void testFindAll() {
//		Mockito.lenient().when(classRepository.findAll()).thenReturn(list);
		List<ClassRoom> newList = classServices.findAll(PageRequest.of(0, 5)).toList();
		assertNotNull(newList);
	}
	
	@Test
	@Order(1)
	void testAddClass() {
		ClassRoom classRoom = ClassRoom.builder().bloc("B").number(4).buildings("Esprit").dateCreation(new Date())
				.build();
		ClassRoom newClassRoom = classServices.addClass(classRoom);
		assertNotNull(newClassRoom.getIdClassRoom());
	}
	
	@Test
	@Order(2)
	void testGetClass() {
		Long id = classRepository.findAll().get(classRepository.findAll().size() -1).getIdClassRoom();
		ClassRoom gottenClass = classServices.getClassRoomById(id);
		assertNotNull(gottenClass);
		log.info(" ClassRoom "+gottenClass.toString());
	}
	
	@Test
	@Order(3)
	void testEditClass() {
		Long id = classRepository.findAll().get(classRepository.findAll().size() -1).getIdClassRoom();
		
		ClassRoom expectedClassRoom = ClassRoom.builder().idClassRoom(id)
				.isStands(true).bloc("B").number(4).buildings("Esprit").dateCreation(new Date())
				.dateModification(new Date()).build();
		
		ClassRoom classRoom  = classServices.getClassRoomById(id);
		classRoom.setIsStands(true);
		
		ClassRoom editedClass = classServices.editClassRoom(classRoom);
		
		assertNotSame(expectedClassRoom.getDateModification(), editedClass.getDateModification());
	}
	
	@Test
	@Order(4)
	void testRemoveClass() {
		Long id = classRepository.findAll().get(classRepository.findAll().size() -1).getIdClassRoom();
		
		classServices.removeClassRomm(id);
		assertNull(classServices.getClassRoomById(id));
	}
}
