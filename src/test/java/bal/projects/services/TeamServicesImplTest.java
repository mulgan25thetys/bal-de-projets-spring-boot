package bal.projects.services;

import static org.junit.Assert.assertNotSame;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.context.SpringBootTest;

import bal.projects.entities.Level;
import bal.projects.entities.Team;
import bal.projects.repositories.TeamRepository;

@ExtendWith(MockitoExtension.class)
@SpringBootTest
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class TeamServicesImplTest {

	private final static Logger log = Logger.getLogger(UserServicesImplTest.class);
	
	@InjectMocks
	TeamsServicesImpl teamService = Mockito.mock(TeamsServicesImpl.class);
	
	@Mock
	TeamRepository teamRepository = Mockito.mock(TeamRepository.class);
	
	Team team = Team.builder().dateCreation(new Date()).level(Level.ANNEE_4).meanPI(0)
			.name("name").build();
	
	@SuppressWarnings("serial")
	List<Team> list = new ArrayList<Team>() {
		{
			add(team);
			add(Team.builder().dateCreation(new Date()).level(Level.ANNEE_4).meanPI(0)
			.name("nameff").build());
		}
	};
	
	@Test
	@Order(0)
	void testAddTeam() {
		Mockito.lenient().when(teamRepository.save(Mockito.any(Team.class))).then(inv -> {
			Team model = inv.getArgument(1, Team.class);
			model.setIdTeam((long)1);
			return model;
		});
		
		log.info("======Avant======="+team.toString());
		Team newTeam = teamService.addTeam(team);
		assertNotSame(newTeam, team);
		log.info("======Apres======="+team.toString());
	}
}
