package bal.projects.services;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertNull;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.apache.log4j.Logger;
import org.junit.Test;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.context.SpringBootTest;

import bal.projects.entities.ProjectsBall;
import bal.projects.repositories.ProjectsBallRepository;

@ExtendWith(MockitoExtension.class)
@SpringBootTest
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class ProjectsBallServicesImplTest {

	private final static Logger log = Logger.getLogger(UserServicesImplTest.class);
	
	@InjectMocks
	ProjectsBallServicesImpl pbServices = Mockito.mock(ProjectsBallServicesImpl.class);
	
	@Mock
	ProjectsBallRepository pbRepository = Mockito.mock(ProjectsBallRepository.class);
	
	ProjectsBall psb = ProjectsBall.builder().editionDate(new Date()).cancelled(false).edition("theme1")
			.title("title").place("place").build();
	
	@SuppressWarnings("serial")
	List<ProjectsBall> list = new ArrayList<ProjectsBall>() {
		{
			add(ProjectsBall.builder().editionDate(new Date()).cancelled(false).edition("theme1")
			.title("title").place("place").build());
		}
	};
	
	@Test
	@Order(0)
	public void testFindAll() {
		Mockito.when(pbRepository.findAll()).thenReturn(list);
//		List<ProjectsBall> newList = pbServices.findAll(PageRequest.of(0, 5)).toList();

//		for (ProjectsBall projectsBall : newList) {
//			log.info(projectsBall.toString());
//		}
		assertNotNull(list);
	}
	
	@Test
	@Order(1)
	public void testAddProjectBall() {
		Mockito.when(pbRepository.save(Mockito.any(ProjectsBall.class))).then(invocation -> {
			ProjectsBall model = invocation.getArgument(0, ProjectsBall.class);
            model.setIdProjectsBall((long)1);
            return model;
		});
		
		log.info("Avant ==> " + psb.toString());
        ProjectsBall projb = pbServices.addProjectBall(psb);
        assertNotSame(projb, psb);
        log.info("Après ==> " + psb.toString());
	}
	
	@Test
	@Order(2)
	public void testGetByid(){
		Mockito.when(pbRepository.findById(Mockito.anyLong())).thenReturn(Optional.of(psb));
		ProjectsBall pBmodel = pbServices.getByid((long)1);
		assertNull(pBmodel);
	}
	
	@Test
	@Order(3)
	public void testEditProjectBall() {
		Mockito.when(pbRepository.save(Mockito.any(ProjectsBall.class))).then(invocation -> {
			ProjectsBall model = invocation.getArgument(0, ProjectsBall.class);
            model.setIdProjectsBall((long)1);
            model.setTitle("New title");
            return model;
		});
		
		log.info("Avant ==> " + psb.toString());
        ProjectsBall projb = pbServices.addProjectBall(psb);
        assertNotSame(projb, psb);
        log.info("Après ==> " + psb.toString());
	}
}
