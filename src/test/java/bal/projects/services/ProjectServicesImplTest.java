package bal.projects.services;

import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageRequest;

import bal.projects.entities.Project;
import bal.projects.entities.Topic;
import bal.projects.repositories.ProjectRepository;

@ExtendWith(MockitoExtension.class)
@SpringBootTest
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class ProjectServicesImplTest {

	private static final Logger log = Logger.getLogger(ProjectServicesImplTest.class);
	
	@Autowired
	IProjectServices projectServicesInterface;
	
	@InjectMocks
	ProjectServicesImpl projectServices = Mockito.mock(ProjectServicesImpl.class);
	
	@Mock
	ProjectRepository projectRepository = Mockito.mock(ProjectRepository.class);
	
	Project project = Project.builder().archived(true).dateCreation(new Date()).name("name project").semester(2)
			.topic(Topic.builder().theme("theme2").build()).build();
	
	@SuppressWarnings("serial")
	List<Project> list = new ArrayList<Project>() {
		{
			add(project);
			add(Project.builder().archived(true).dateCreation(new Date()).name("name project2").semester(2)
					.topic(Topic.builder().theme("theme2").build()).build());
			add(Project.builder().archived(true).dateCreation(new Date()).name("name project3").semester(2)
					.topic(Topic.builder().theme("theme3").build()).build());
		}
	};
			
	@Test
	@Order(0)
	void testFindAll() {
		List<Project> gottenList = projectServicesInterface.findAll(PageRequest.of(2, 5)).toList();
		assertNotNull(gottenList);
	}
	
	@Test
	@Order(1)
	void testAddProject() {
		Mockito.lenient().when(projectRepository.save(Mockito.any(Project.class))).then(inv -> {
			Project model = inv.getArgument(1, Project.class);
			model.setIdProject((long)1);
			return model;
		});
		log.info("=========== Avant =========="+project.toString());
		assertNotNull(project);
		log.info("=========== Après =========="+project.toString());
	}
}
