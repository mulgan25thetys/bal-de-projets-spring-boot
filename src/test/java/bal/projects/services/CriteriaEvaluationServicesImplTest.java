package bal.projects.services;

import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.junit.Test;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import bal.projects.entities.CriteriaEvaluation;
import bal.projects.repositories.CriteriaEvaluationRepository;

@ExtendWith(MockitoExtension.class)
@SpringBootTest
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class CriteriaEvaluationServicesImplTest {

	private static final Logger log = Logger.getLogger(CriteriaEvaluationServicesImplTest.class);

	@Autowired
	ICriteriaEvaluationServices icriteriaService;
	
	@Mock
	CriteriaEvaluationRepository criteriaRepo = Mockito.mock(CriteriaEvaluationRepository.class);
	
	@SuppressWarnings("serial")
	List<CriteriaEvaluation> list = new ArrayList<CriteriaEvaluation>() {
		{
			add(CriteriaEvaluation.builder().criteria("new criteria")
					.dateCreation(new Date()).level(1).optionnal(true).build());
			add(CriteriaEvaluation.builder().criteria("new  2criteria")
					.dateCreation(new Date()).level(1).optionnal(false).build());
		}
	};
	
	@Test
	@Order(0)
	public void testFindAll() {
		List<CriteriaEvaluation> criterias = criteriaRepo.findAll();
		
		assertNotNull(criterias);
		for (CriteriaEvaluation criteriaEvaluation : criterias) {
			log.info(criteriaEvaluation.toString());
		}
	}
	
//	@Test
//	@Order(1)
//	public void testAddCriteria() {
//		CriteriaEvaluation criteria = CriteriaEvaluation.builder().criteria("Lorem Ipsum is simply dummy text of the printing and typesetting")
//				.dateCreation(new Date()).level(1).optionnal(true).build();
//		
//		CriteriaEvaluation addedCriteria = icriteriaService.addCriteriaEvaluation(criteria);
//		
//		assertNotNull(addedCriteria.getIdCriteria());
//	}
//	
//	@Test
//	@Order(2)
//	public void testEditCriteria() {
//		Long id = icriteriaService.getAll().get(icriteriaService.getAll().size() -1).getIdCriteria();
//		
//		CriteriaEvaluation expectedCriteria = CriteriaEvaluation.builder().idCriteria(id).criteria("Lorem Ipsum is simply dummy text of the printing and typesetting")
//				.dateCreation(new Date()).level(2).optionnal(true).build();
//		
//		CriteriaEvaluation criteria = icriteriaService.getCriteriaEvaluation(id);
//		criteria.setLevel(2);
//		
//		CriteriaEvaluation editedCriteria = icriteriaService.editCriteriaEvaluation(criteria);
//		
//		assertSame(expectedCriteria.getLevel(), editedCriteria.getLevel());
//	}
//	
//	@Test
//	@Order(3)
//	public void testGetCriteria() {
//		Long id = criteriaRepo.findAll().get(criteriaRepo.findAll().size() -1).getIdCriteria();
//		CriteriaEvaluation gottenCriteria = icriteriaService.getCriteriaEvaluation(id);
//		
//		assertNotNull(gottenCriteria);
//		
//		log.info(" Criteria Evaluation "+gottenCriteria.toString());
//	}
//	
//	@Test
//	@Order(4)
//	public void testRemoveCriteria() {
//		Long id = criteriaRepo.findAll().get(criteriaRepo.findAll().size() -1).getIdCriteria();
//		icriteriaService.removeCriteriaEvaluation(id);
//		
//		assertNull(criteriaRepo.findById(id));
//	}
}
