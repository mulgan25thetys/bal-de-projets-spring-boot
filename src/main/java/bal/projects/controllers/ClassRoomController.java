package bal.projects.controllers;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import bal.projects.entities.ClassRoom;
import bal.projects.payload.response.MessageResponse;
import bal.projects.repositories.ClassRoomRepository;
import bal.projects.services.IClassRoomServices;

@RestController
@RequestMapping("/classroom")
public class ClassRoomController {

	@Autowired
	IClassRoomServices classRoomServices;
	
	@Autowired
	ClassRoomRepository classRepository;
	
	@GetMapping("/find-all")
	@ResponseBody
	public Page<ClassRoom> findAllClassRooms(@RequestParam(defaultValue = "0") int page, @RequestParam(defaultValue = "3") int size){
		Pageable paging = PageRequest.of(page, size);
		return classRoomServices.findAll(paging);
	}
	
	@PreAuthorize("hasAnyRole('ROLE_RESPONSABLE_OPTION','ROLE_COMITE_ORGANISATION')")
	@SuppressWarnings("all")
	@PostMapping("/add-classroom")
	@ResponseBody
	public ResponseEntity<Object> addClassRoom(@Valid @RequestBody ClassRoom classRoom) {
		
		if(Boolean.TRUE.equals(classRepository.existsByBlocAndNumber(classRoom.getBloc(), classRoom.getNumber()))) {
			return ResponseEntity.badRequest().body(new MessageResponse("This classRoom is already created!"));
		}
		
		return ResponseEntity.ok().body(classRoomServices.addClass(classRoom));
	}
	
	@PreAuthorize("hasAnyRole('ROLE_COMITE_ORGANISATION')")
	@DeleteMapping("/delete-classroom/{id}")
	@ResponseBody
	public ResponseEntity<Object> deleteClassRoom(@PathVariable("id") Long id){
		if(Boolean.TRUE.equals(classRepository.existsById(id))) {
			classRoomServices.removeClassRomm(id);
			return ResponseEntity.ok().body(new MessageResponse("An classRoom has been deleted!"));
		}
		return ResponseEntity.badRequest().body(new MessageResponse("An error has been occured, this entry "
				+ "does not exist!"));
	}
	
	@PreAuthorize("hasAnyRole('ROLE_RESPONSABLE_OPTION','ROLE_COMITE_ORGANISATION')")
	@SuppressWarnings("all")
	@PutMapping("/edit-classroom")
	@ResponseBody
	public ResponseEntity<Object> editClassRoom(@Valid @RequestBody ClassRoom classRoom) {
		
		if(Boolean.TRUE.equals(classRepository.existsByBlocAndNumber(classRoom.getBloc(), classRoom.getNumber()))) {
			Optional<ClassRoom> existedClass = classRepository.findByBlocAndNumber(classRoom.getBloc(), classRoom.getNumber());
			
			if(existedClass.isPresent() && existedClass.get().getIdClassRoom() != classRoom.getIdClassRoom()) {
				return ResponseEntity.badRequest().body(new MessageResponse("This classRoom is already existed!"));
			}
		}
		
		return ResponseEntity.ok().body(classRoomServices.editClassRoom(classRoom));
	}
	
	@PreAuthorize("hasAnyRole('ROLE_RESPONSABLE_OPTION','ROLE_COMITE_ORGANISATION','ROLE_ETUDIANT','ROLE_ENSEIGNANT')")
	@GetMapping("/get-classroom/{id}")
	@ResponseBody
	public ResponseEntity<Object> getClassRoom(@PathVariable("id") Long id) {
		
		if(Boolean.FALSE.equals(classRepository.existsById(id))) {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new MessageResponse("The requested class does not exist!"));
		}
		
		return ResponseEntity.ok().body(classRoomServices.getClassRoomById(id));
	}
}
