package bal.projects.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import bal.projects.entities.Notification;
import bal.projects.repositories.NotificationRepository;

@RestController
@RequestMapping("/notification")
public class NotificationController {

	@Autowired
	NotificationRepository notificationRepository;
	
	@PreAuthorize("hasAnyRole('ROLE_RESPONSABLE_OPTION','ROLE_COMITE_ORGANISATION','ROLE_ETUDIANT','ROLE_ENSEIGNANT')")
	@GetMapping("/list-all")
	@ResponseBody
	public Page<Notification> findAll(@RequestParam(defaultValue = "0") int page, @RequestParam(defaultValue = "3") int size){
		Pageable paging = PageRequest.of(page, size);
		return notificationRepository.findAll(paging);
	}
}
