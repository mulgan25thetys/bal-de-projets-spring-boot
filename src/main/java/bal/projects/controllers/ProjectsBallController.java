package bal.projects.controllers;

import java.util.Calendar;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import bal.projects.entities.ProjectsBall;
import bal.projects.payload.response.MessageResponse;
import bal.projects.repositories.ProjectsBallRepository;
import bal.projects.repositories.TeamRepository;
import bal.projects.services.IProjectsBallServices;

@RestController
@RequestMapping("/projects-ball")
public class ProjectsBallController {

	@Autowired
	IProjectsBallServices projectsBallServices;
	
	@Autowired
	ProjectsBallRepository pBRepo;
	
	@Autowired
	ProjectsBallRepository projectsBRepository;
	
	@Autowired
	TeamRepository teamRepository;
	
	@PreAuthorize("hasAnyRole('ROLE_RESPONSABLE_OPTION','ROLE_COMITE_ORGANISATION','ROLE_ETUDIANT','ROLE_ENSEIGNANT')")
	@GetMapping("/find-all")
	@ResponseBody
	public Page<ProjectsBall> findAll(@RequestParam(defaultValue = "0") int page, @RequestParam(defaultValue = "3") int size){
		Pageable paging = PageRequest.of(page, size);
		return projectsBallServices.findAll(paging);
	}
	
	@PreAuthorize("hasAnyRole('ROLE_COMITE_ORGANISATION')")
	@SuppressWarnings("all")
	@PostMapping("/add-projects-ball")
	@ResponseBody
	public ResponseEntity<Object> addProjectsBall(@Valid @RequestBody ProjectsBall projectBall) {
		Optional<ProjectsBall> existedProjectsB = projectsBRepository.getProjectBallOfYear(Calendar.getInstance().get(Calendar.YEAR));
		
		if(existedProjectsB.isPresent()) {
			return ResponseEntity.badRequest().body(new MessageResponse("Projects Ball of this "
						+ "year has already created!"));
		}
		
		if(Boolean.TRUE.equals(projectsBRepository.existsByEdition(projectBall.getEdition()))) {
			return ResponseEntity.badRequest().body(new MessageResponse("This Edition is already created!"));
		}
		
		if(Boolean.TRUE.equals(projectsBRepository.existsByTitle(projectBall.getTitle()))) {
			return ResponseEntity.badRequest().body(new MessageResponse("This title is already taken!"));
		}
		
		return ResponseEntity.ok().body(projectsBallServices.addProjectBall(projectBall));
	}
	
	@PreAuthorize("hasAnyRole('ROLE_COMITE_ORGANISATION')")
	@SuppressWarnings("all")
	@PutMapping("/edit-projects-ball")
	@ResponseBody
	public ProjectsBall editProjectsBall(@Valid @RequestBody ProjectsBall projectBall) {
		return projectsBallServices.editProjectsBall(projectBall);
	}
	
	@PreAuthorize("hasAnyRole('ROLE_RESPONSABLE_OPTION','ROLE_COMITE_ORGANISATION','ROLE_ETUDIANT','ROLE_ENSEIGNANT')")
	@GetMapping("/get-projects-ball/{id}")
	public ResponseEntity<Object> getProjectsBall(@PathVariable("id") Long id) {
		
		if(Boolean.FALSE.equals(pBRepo.existsById(id))) {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body("This project ball not found!");
		}
		return ResponseEntity.ok().body(projectsBallServices.getByid(id));
	}
	
	@PreAuthorize("hasAnyRole('ROLE_RESPONSABLE_OPTION','ROLE_COMITE_ORGANISATION','ROLE_ETUDIANT','ROLE_ENSEIGNANT')")
	@GetMapping("/get-affected-teams/{id}")
	public ResponseEntity<Object> getProjectsBallAffectedTeams(@PathVariable("id") Long id) {
		
		if(Boolean.TRUE.equals(projectsBRepository.existsById(id))) {
			return ResponseEntity.ok().body(projectsBallServices.getByid(id));
		}
		return ResponseEntity.badRequest().body(new MessageResponse("This projects Ball does not exist!"));
	}
	
	@PreAuthorize("hasAnyRole('ROLE_RESPONSABLE_OPTION','ROLE_COMITE_ORGANISATION','ROLE_ETUDIANT','ROLE_ENSEIGNANT')")
	@GetMapping("/get-specific-team/{idpb}/{idteam}")
	public ResponseEntity<Object> findProjectsBallAffectedTeam(@PathVariable("idpb") Long idpb,@PathVariable("idteam") Long idteam) {
		
		if(Boolean.FALSE.equals(projectsBRepository.existsById(idpb))) {
			return ResponseEntity.badRequest().body(new MessageResponse("This projects Ball does not exist!"));
		}
		
		if(Boolean.FALSE.equals(teamRepository.existsById(idteam))) {
			return ResponseEntity.badRequest().body(new MessageResponse("This Team does not exist!"));
		}
		
		return ResponseEntity.ok().body(projectsBallServices.findAffectedTeam(idpb, idteam));
	}
	
	@PreAuthorize("hasAnyRole('ROLE_COMITE_ORGANISATION')")
	@PutMapping("/canceled/{id}")
	public ResponseEntity<Object> canceledProjectB(@PathVariable("id") Long id){
		
		if(Boolean.FALSE.equals(projectsBRepository.existsById(id))) {
			return ResponseEntity.badRequest().body(new MessageResponse("This projects Ball is not found!"));
		}
		
		projectsBallServices.canceledProjectBall(id);
		
		return ResponseEntity.ok().body(new MessageResponse("The Projects ball of "+Calendar.getInstance().get(Calendar.YEAR)+" is canceled!"));
	}
}
