package bal.projects.controllers;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import bal.projects.entities.Project;
import bal.projects.entities.Vote;
import bal.projects.payload.response.MessageResponse;
import bal.projects.repositories.ProjectRepository;
import bal.projects.repositories.TeamRepository;
import bal.projects.services.IProjectServices;

@RestController
@RequestMapping("/projects")
public class ProjectController {
	
	@Autowired
	IProjectServices projectsServices;
	
	@Autowired
	ProjectRepository projectRepository;
	
	@Autowired
	TeamRepository teamRepository;
	
	@PreAuthorize("hasAnyRole('ROLE_RESPONSABLE_OPTION','ROLE_ENSEIGNANT','ROLE_ETUDIANT','ROLE_COMITE_ORGANISATION')")
	@GetMapping("/find-all")
	@ResponseBody
	public Page<Project> findAll(@RequestParam(defaultValue = "0") int page, @RequestParam(defaultValue = "3") int size){
		Pageable paging = PageRequest.of(page, size);
		return projectsServices.findAll(paging);
	}
	
	@PreAuthorize("hasAnyRole('ROLE_RESPONSABLE_OPTION','ROLE_ENSEIGNANT','ROLE_ETUDIANT','ROLE_COMITE_ORGANISATION')")
	@GetMapping("/get-all")
	@ResponseBody
	public List<Project> getAll(){
		
		return projectRepository.findAll();
	}
	
	@PreAuthorize("hasAnyRole('ROLE_RESPONSABLE_OPTION','ROLE_ENSEIGNANT','ROLE_ETUDIANT','ROLE_COMITE_ORGANISATION')")
	@GetMapping("/find-project/{id}")
	@ResponseBody
	public ResponseEntity<Object> findProject(@PathVariable("id") Long id){
		
		if(Boolean.FALSE.equals(projectRepository.existsById(id))) {
			return ResponseEntity.badRequest().body(new MessageResponse("Project does not exist!"));
		}
		return ResponseEntity.ok().body(projectsServices.getProject(id));
	}
	
	@PreAuthorize("hasAnyRole('ROLE_RESPONSABLE_OPTION','ROLE_ETUDIANT')")
	@PostMapping("/add-projet/{idTeam}/{idTopic}")
	@SuppressWarnings("all")
	@ResponseBody
	public ResponseEntity<Object> addProject(@PathVariable("idTeam") Long idTeam,@PathVariable("idTopic") Long idTopic,@Valid @RequestBody Project project){
		
		Optional<Project> existedproject = projectRepository.existsByTeam(idTeam);
		
		if(Boolean.TRUE.equals(projectRepository.existsByName(project.getName()))) {
			return ResponseEntity.badRequest().body(new MessageResponse("This project'name is already exists!"));
		}
		
		if(existedproject.isPresent()) {
			return ResponseEntity.badRequest().body(new MessageResponse("This project is already added!"));
		}
		
		return ResponseEntity.ok().body(projectsServices.addProject(idTeam, idTopic, project));
	}
	
	@PreAuthorize("hasAnyRole('ROLE_RESPONSABLE_OPTION','ROLE_ETUDIANT')")
	@PostMapping("/upload/{id}")
	  public ResponseEntity<MessageResponse> uploadFile(@RequestParam("file") MultipartFile file,@PathVariable("id") Long id) {
	    String message = "";
	    if(Boolean.FALSE.equals(projectRepository.existsById(id))) {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new MessageResponse("This project not Found!"));
			
		}
	    try {
	      projectsServices.saveFiles(file,id);
	      message = "Uploaded the file successfully: " + file.getOriginalFilename();
	      return ResponseEntity.status(HttpStatus.OK).body(new MessageResponse(message));
	    } catch (Exception e) {
	      message = "Could not upload the file: " + file.getOriginalFilename() + "!";
	      return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(new MessageResponse(message));
	    }
	  }
	
	@PreAuthorize("hasAnyRole('ROLE_RESPONSABLE_OPTION','ROLE_ETUDIANT')")
	@PutMapping("/edit-projet")
	@SuppressWarnings("all")
	@ResponseBody
	public ResponseEntity<Object> editProject(@RequestBody Project project){
		
		if(Boolean.TRUE.equals(projectRepository.existsByName(project.getName()))) {
			Optional<Project> existedProject = projectRepository.findByName(project.getName());
			
			if(existedProject.isPresent() && !existedProject.get().getIdProject().equals(project.getIdProject())) {
				return ResponseEntity.badRequest().body(new MessageResponse("This project'name is already taken!"));
			}
		}
		
		return ResponseEntity.ok().body(projectsServices.editProject(project));
	}
	
	@PreAuthorize("hasAnyRole('ROLE_RESPONSABLE_OPTION','ROLE_ETUDIANT')")
	@DeleteMapping("/remove-project/{id}")
	@ResponseBody
	public ResponseEntity<Object> removeProject(@PathVariable("id") Long id){
		
		if(Boolean.FALSE.equals(projectRepository.existsById(id))) {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new MessageResponse("This project does not exist!"));
			
		}
		projectsServices.removeProject(id);
		return ResponseEntity.ok().body(new MessageResponse("An project has been deleted!"));
	}
	
	@PreAuthorize("hasAnyRole('ROLE_RESPONSABLE_OPTION','ROLE_ENSEIGNANT','ROLE_ETUDIANT','ROLE_COMITE_ORGANISATION')")
	@GetMapping("/files/{filename:.+}")
	@ResponseBody
	public ResponseEntity<Object> getFileForProfile(@PathVariable String filename) throws IOException {
	    Resource file = projectsServices.load(filename);
	    if(file == null) {
	    	return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new MessageResponse("This project's file not found!"));
	    }
	    Path path = file.getFile()
              .toPath();
	    
	    return ResponseEntity.ok()
              .header(HttpHeaders.CONTENT_TYPE, Files.probeContentType(path))
              .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + file.getFilename() + "\"")
              .body(file);
	}
	
	@PreAuthorize("hasAnyRole('ROLE_ETUDIANT')")
	@PutMapping("/vote/{id}")
	@SuppressWarnings("all")
	@ResponseBody
	public ResponseEntity<Object> voteProject(@PathVariable("id") Long id,@RequestBody Vote vote){
		if(Boolean.FALSE.equals(projectRepository.existsById(id))) {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new MessageResponse("Operation failed! because the project does not exist!"));
		}
		return ResponseEntity.ok().body(projectsServices.votesProject(vote, id));
	}
	
	@PreAuthorize("hasAnyRole('ROLE_RESPONSABLE_OPTION','ROLE_COMITE_ORGANISATION','ROLE_ETUDIANT','ROLE_ENSEIGNANT')")
	@GetMapping("/get-team-by-project/{id}")
	@ResponseBody
	public ResponseEntity<Object> getFileForProfile(@PathVariable("id") Long id) {
		
		if(Boolean.FALSE.equals(projectRepository.existsById(id))) {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new MessageResponse("It is non existent project!"));
		}
		return ResponseEntity.ok().body(projectsServices.getTeamByProject(id));
	}
}
