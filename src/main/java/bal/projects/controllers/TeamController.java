package bal.projects.controllers;

import java.util.Calendar;
import java.util.List;
import java.util.Optional;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import bal.projects.entities.Agreement;
import bal.projects.entities.ProjectsBall;
import bal.projects.entities.Team;
import bal.projects.payload.response.MessageResponse;
import bal.projects.repositories.ProjectsBallRepository;
import bal.projects.repositories.TeamRepository;
import bal.projects.repositories.UserRepository;
import bal.projects.services.ITeamServices;

@RestController
@RequestMapping("/teams")
public class TeamController {

	@Autowired
	ProjectsBallRepository projectsBRepository;
	
	@Autowired
	ITeamServices teamServices;
	
	@Autowired
	TeamRepository teamRepository;
	
	@Autowired
	UserRepository userRepository;
	
	@Autowired
	ProjectsBallRepository pBRepository;
	
	@PreAuthorize("hasAnyRole('ROLE_RESPONSABLE_OPTION','ROLE_COMITE_ORGANISATION','ROLE_ETUDIANT','ROLE_ENSEIGNANT')")
	@GetMapping("/find-all")
	@ResponseBody
	public Page<Team> findAll(@RequestParam(defaultValue = "0") int page, @RequestParam(defaultValue = "3") int size){
		Pageable paging = PageRequest.of(page, size);
		return teamServices.findAll(paging);
	}
	
	@PreAuthorize("hasAnyRole('ROLE_RESPONSABLE_OPTION','ROLE_COMITE_ORGANISATION','ROLE_ETUDIANT','ROLE_ENSEIGNANT')")
	@GetMapping("/get-all")
	@ResponseBody
	public List<Team> getAll(){
			return teamRepository.findAll();
	}
	
	@PreAuthorize("hasAnyRole('ROLE_RESPONSABLE_OPTION','ROLE_COMITE_ORGANISATION','ROLE_ETUDIANT','ROLE_ENSEIGNANT')")
	@GetMapping("/get-my-team/{myId}")
	@ResponseBody
	public ResponseEntity<Object> getMyTeam(@PathVariable("myId") Long IdUser){
		if(Boolean.FALSE.equals(userRepository.existsById(IdUser))) {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new MessageResponse("User cannot be found!"));
		}
		return ResponseEntity.ok().body(teamRepository.getMyTeam(IdUser));
	}
	
	@PreAuthorize("hasAnyRole('ROLE_RESPONSABLE_OPTION','ROLE_COMITE_ORGANISATION','ROLE_ETUDIANT','ROLE_ENSEIGNANT')")
	@GetMapping("/find-by-id/{id}")
	@ResponseBody
	public ResponseEntity<Object> getById(@PathVariable("id") Long id) {
		if(Boolean.FALSE.equals(teamRepository.existsById(id))) {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new MessageResponse("Team non existant!"));
		}
		
		return ResponseEntity.ok().body(teamServices.getTeam(id));
	}
	
	@PreAuthorize("hasAnyRole('ROLE_RESPONSABLE_OPTION','ROLE_COMITE_ORGANISATION','ROLE_ETUDIANT','ROLE_ENSEIGNANT')")
	@SuppressWarnings("all")
	@PostMapping("/add-team")
	@ResponseBody
	public ResponseEntity<Object> addTeam(@RequestBody Team team) {
		
		if(Boolean.TRUE.equals(teamRepository.existsByName(team.getName()))) {
			return ResponseEntity.badRequest().body(new MessageResponse("This team's name is already exists"));
		}
		
		if(team.getIdsMembers() == null || team.getIdsMembers().isEmpty()) {
			return ResponseEntity.badRequest().body(new MessageResponse("There is no member in that Team!"));
		}
		
		
		return ResponseEntity.ok().body(teamServices.addTeam(team));
	}
	
	@PreAuthorize("hasAnyRole('ROLE_RESPONSABLE_OPTION','ROLE_COMITE_ORGANISATION','ROLE_ETUDIANT','ROLE_ENSEIGNANT')")
	@PutMapping("/edit-team")
	@SuppressWarnings("all")
	@ResponseBody
	public ResponseEntity<Object> editTeam(@RequestBody Team team) {
		if(Boolean.TRUE.equals(teamRepository.existsByName(team.getName()))) {
			
			Optional<Team> existedTeam = teamRepository.findByName(team.getName());
			if(existedTeam.isPresent() && existedTeam.get().getIdTeam() != team.getIdTeam()){
				return ResponseEntity.badRequest().body(new MessageResponse("This team's name is already taken"));
			}
		}
	
		return ResponseEntity.ok().body(teamServices.editTeam(team));
	}
	
	@PreAuthorize("hasAnyRole('ROLE_ENSEIGNANT')")
	@PutMapping("/set-agreements/{idtutor}/{idteam}")
	@SuppressWarnings("all")
	@ResponseBody
	public Team setTutorsAgreements(@RequestBody Agreement agreement,@PathVariable("idtutor") Long idTutor,@PathVariable("idteam") Long idTeam) {
		return teamServices.setTutorsAgreements(idTutor,idTeam, agreement);
	}
	
	@PreAuthorize("hasAnyRole('ROLE_RESPONSABLE_OPTION')")
	@PutMapping("/add-to-projects-ball/{idteam}")
	@ResponseBody
	public ResponseEntity<Object> addToProjectBall(@PathVariable("idteam") Long idteam){
		
		Optional<ProjectsBall> existedProjectsB = projectsBRepository.getProjectBallOfYear(Calendar.getInstance().get(Calendar.YEAR));
		Optional<Team> existedTeam = teamRepository.findById(idteam);
		
		if(!existedProjectsB.isPresent() || Boolean.TRUE.equals(existedProjectsB.get().getCancelled())) {
			return ResponseEntity.badRequest().body(new MessageResponse("Projects Ball of this "
						+ "year hasn't already created!"));
		}
		
		if(Boolean.FALSE.equals(teamRepository.existsById(idteam))) {
			return ResponseEntity.badRequest().body(new MessageResponse("This team does not exist!"));
		}
		
		if(existedTeam.isPresent() && existedTeam.get().getProject() == null) {
			return ResponseEntity.badRequest().body(new MessageResponse("This team cannot be affected, because it has not project!"));
		}
		
		return ResponseEntity.ok().body(teamServices.addToProjectsBall(idteam,true));
	}
	
	@PreAuthorize("hasAnyRole('ROLE_RESPONSABLE_OPTION')")
	@PutMapping("/remove-from-projects-ball/{idteam}")
	@ResponseBody
	public ResponseEntity<Object> removeFromProjectBall(@PathVariable("idteam") Long idteam){
		
		if(Boolean.FALSE.equals(teamRepository.existsById(idteam))) {
			return ResponseEntity.badRequest().body(new MessageResponse("This team does not exist!"));
		}
		
		return ResponseEntity.ok().body(teamServices.addToProjectsBall(idteam,false));
	}
	
	@PreAuthorize("hasAnyRole('ROLE_RESPONSABLE_OPTION')")
	@DeleteMapping("/remove-team/{idteam}")
	@ResponseBody
	public ResponseEntity<Object> removeTeam(@PathVariable("idteam") Long idteam){
		
		if(Boolean.FALSE.equals(teamRepository.existsById(idteam))) {
			return ResponseEntity.badRequest().body(new MessageResponse("This team is not exist!"));
		}
		
		teamServices.removeTeam(idteam);
		return ResponseEntity.ok().body(new MessageResponse("One team has been deleted!"));
	}
}
