package bal.projects.controllers;

import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import bal.projects.entities.CriteriaEvaluation;
import bal.projects.payload.response.MessageResponse;
import bal.projects.repositories.CriteriaEvaluationRepository;
import bal.projects.services.ICriteriaEvaluationServices;

@RestController
@RequestMapping("/criteria-evaluations")
public class CriteriaEvaluationController {

	@Autowired
	ICriteriaEvaluationServices criteriaServices;
	
	@Autowired
	CriteriaEvaluationRepository criteriaRepository;
	
	@PreAuthorize("hasAnyRole('ROLE_RESPONSABLE_OPTION','ROLE_COMITE_ORGANISATION','ROLE_ENSEIGNANT','ROLE_ETUDIANT')")
	@GetMapping("/find-all")
	@ResponseBody
	public Page<CriteriaEvaluation> findAll(@RequestParam(defaultValue = "0") int page, @RequestParam(defaultValue = "3") int size){
		Pageable paging = PageRequest.of(page, size);
		return criteriaServices.findAll(paging);
	}
	
	@PreAuthorize("hasAnyRole('ROLE_COMITE_ORGANISATION')")
	@SuppressWarnings("all")
	@PostMapping("/add-criteria")
	@ResponseBody
	public ResponseEntity<Object> addCriteria(@Valid @RequestBody CriteriaEvaluation criteria) {
		
		if(Boolean.TRUE.equals(criteriaRepository.existsByCriteria(criteria.getCriteria()))) {
			return ResponseEntity.badRequest().body(new MessageResponse("This Evaluation Criteria's criteria is already created!"));
		}
		
		if(Boolean.TRUE.equals(criteriaRepository.existsByLevel(criteria.getLevel()))) {
			return ResponseEntity.badRequest().body(new MessageResponse("This Evaluation Criteria's level is already exist!"));
		}
		
		return ResponseEntity.ok().body(criteriaServices.addCriteriaEvaluation(criteria));
	}
	
	@PreAuthorize("hasAnyRole('ROLE_COMITE_ORGANISATION')")
	@SuppressWarnings("all")
	@PutMapping("/edit-criteria")
	@ResponseBody
	public ResponseEntity<Object> editCriteria(@Valid @RequestBody CriteriaEvaluation criteria) {
		
		if(Boolean.TRUE.equals(criteriaRepository.existsByCriteria(criteria.getCriteria()))) {
			
			Optional<CriteriaEvaluation> existedCriteria = criteriaRepository.findByCriteria(criteria.getCriteria());
			
			if(existedCriteria.isPresent() && !existedCriteria.get().getIdCriteria().equals(criteria.getIdCriteria())) {
				return ResponseEntity.badRequest().body(new MessageResponse("This Evaluation Criteria is already exist!"));
			}
		}
		
		return ResponseEntity.ok().body(criteriaServices.addCriteriaEvaluation(criteria));
	}
	
	@PreAuthorize("hasAnyRole('ROLE_RESPONSABLE_OPTION','ROLE_COMITE_ORGANISATION','ROLE_ENSEIGNANT','ROLE_ETUDIANT')")
	@GetMapping("/get-criteria/{id}")
	@ResponseBody
	public ResponseEntity<Object> getCriteria(@PathVariable("id") Long id){
		
		if(Boolean.FALSE.equals(criteriaRepository.existsById(id))) {
			return ResponseEntity.badRequest().body(new MessageResponse("This Evaluation Criteria does not exists!"));
		}
		
		return ResponseEntity.ok().body(criteriaServices.getCriteriaEvaluation(id));
	}
	
	@PreAuthorize("hasAnyRole('ROLE_COMITE_ORGANISATION')")
	@DeleteMapping("/remove-criteria/{id}")
	@ResponseBody
	public ResponseEntity<Object> removeCriteria(@PathVariable("id") Long id){
		
		if(Boolean.FALSE.equals(criteriaRepository.existsById(id))) {
			return ResponseEntity.badRequest().body(new MessageResponse("This Evaluation Criteria is non-existent!"));
		}
		
		criteriaServices.removeCriteriaEvaluation(id);
		return ResponseEntity.ok().body(new MessageResponse("One Evaluation Criteria has been deleted!"));
	}
}
