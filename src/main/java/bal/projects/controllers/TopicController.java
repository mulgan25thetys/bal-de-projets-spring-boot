package bal.projects.controllers;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.batch.core.BatchStatus;
import org.springframework.batch.core.JobParametersInvalidException;
import org.springframework.batch.core.repository.JobExecutionAlreadyRunningException;
import org.springframework.batch.core.repository.JobInstanceAlreadyCompleteException;
import org.springframework.batch.core.repository.JobRestartException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import bal.projects.BatchLauncher;
import bal.projects.entities.Topic;
import bal.projects.payload.response.MessageResponse;
import bal.projects.repositories.TopicRepository;
import bal.projects.services.ITopicServices;

@RestController
@RequestMapping("/topic")
public class TopicController {

	private static final String MESSAGE_4_TOPIC_ERROR = "This topic does not exist!"; 
	
	private static final Logger log = Logger.getLogger(TopicController.class);
	
	@Autowired
	ITopicServices topicServices;
	
	@Autowired
	TopicRepository topicRepository;
	
	@Autowired
    private BatchLauncher batchLauncher;
	
	@GetMapping("/launch")
    public BatchStatus load() throws JobParametersInvalidException, JobExecutionAlreadyRunningException, JobRestartException, JobInstanceAlreadyCompleteException {
        log.info("Batch demarr&#xE9; &#xE0; la demande");
        return batchLauncher.run();
    }
	
	@PreAuthorize("hasAnyRole('ROLE_COMITE_ORGANISATION','ROLE_RESPONSABLE_OPTION','ROLE_ENSEIGNANT','ROLE_ETUDIANT')")
	@GetMapping("/find-all")
	@ResponseBody
	public Page<Topic> findAll(@RequestParam(defaultValue = "0") int page, @RequestParam(defaultValue = "3") int size) {
        Pageable paging = PageRequest.of(page, size);
		return topicServices.findAll(paging);
	}
	
	@PreAuthorize("hasAnyRole('ROLE_COMITE_ORGANISATION','ROLE_RESPONSABLE_OPTION','ROLE_ENSEIGNANT','ROLE_ETUDIANT')")
	@GetMapping("/get-all")
	@ResponseBody
	public List<Topic> getAll() {
        
		return topicRepository.findAll();
	}
	
	@PreAuthorize("hasAnyRole('ROLE_COMITE_ORGANISATION','ROLE_RESPONSABLE_OPTION','ROLE_ENSEIGNANT','ROLE_ETUDIANT')")
	@GetMapping("/find-by-id/{id}")
	@ResponseBody
	public ResponseEntity<Object> findById(@PathVariable("id") Long id) {
		
		if(Boolean.FALSE.equals(topicRepository.existsById(id))) {
			return ResponseEntity.badRequest().body(new MessageResponse(MESSAGE_4_TOPIC_ERROR));
		}
		
		return ResponseEntity.ok().body(topicServices.getTopic(id));
	}
	
	@PreAuthorize("hasAnyRole('ROLE_COMITE_ORGANISATION','ROLE_RESPONSABLE_OPTION')")
	@PostMapping("/add-topic")
	@SuppressWarnings("all")
	@ResponseBody
	public ResponseEntity<Object> addTopic(@Valid @RequestBody Topic topic) {
		
		if(Boolean.TRUE.equals(topicRepository.existsByTheme(topic.getTheme()))) {
			return ResponseEntity.badRequest().body(new MessageResponse("This topic's theme already created!"));
		}
		
		return ResponseEntity.ok().body(topicServices.addTopic(topic));
	}
	
	@PreAuthorize("hasAnyRole('ROLE_COMITE_ORGANISATION','ROLE_RESPONSABLE_OPTION')")
	@PutMapping("/edit-topic")
	@SuppressWarnings("all")
	@ResponseBody
	public ResponseEntity<Object> editTopic(@Valid @RequestBody Topic topic) {
		if(Boolean.TRUE.equals(topicRepository.existsByTheme(topic.getTheme()))) {
			
			Optional<Topic> existedTopic = topicRepository.findByTheme(topic.getTheme());
			if(existedTopic.isPresent() && existedTopic.get().getIdTopic() != topic.getIdTopic()) {
				return ResponseEntity.badRequest().body(new MessageResponse("This topic's theme already exist!"));
			}
		}
		
		return ResponseEntity.ok().body(topicServices.addTopic(topic));
	}
	
	@PreAuthorize("hasAnyRole('ROLE_COMITE_ORGANISATION','ROLE_RESPONSABLE_OPTION')")
	@DeleteMapping("/delete-by-id/{id}")
	@ResponseBody
	public ResponseEntity<Object> deleteById(@PathVariable("id") Long id) {
		
		if(Boolean.TRUE.equals(topicRepository.existsById(id))) {
			topicServices.removeTopic(id);
			return ResponseEntity.ok().body(new MessageResponse("A Topic has been deleted!"));
		}
		
		return ResponseEntity.badRequest().body(new MessageResponse(MESSAGE_4_TOPIC_ERROR));
	}
	
	@PreAuthorize("hasAnyRole('ROLE_COMITE_ORGANISATION','ROLE_RESPONSABLE_OPTION','ROLE_ENSEIGNANT','ROLE_ETUDIANT')")
	@GetMapping("/get-projects-by-topic/{id}")
	@ResponseBody
	public ResponseEntity<Object> getProjectsByTopic(@PathVariable("id") Long id) {
		
		if(Boolean.FALSE.equals(topicRepository.existsById(id))) {
			return ResponseEntity.badRequest().body(new MessageResponse(MESSAGE_4_TOPIC_ERROR));
		}
		
		return ResponseEntity.ok().body(topicServices.getProjectsForByTopic(id));
	}
}
