package bal.projects.services;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import bal.projects.entities.Project;
import bal.projects.entities.Topic;

public interface ITopicServices {

	Page<Topic> findAll(Pageable pageable);
	
	Topic addTopic(Topic topic);
	
	Topic editTopic(Topic topic);
	
	void removeTopic(Long id);
	
	Topic getTopic(Long id);
	
	List<Project> getProjectsForByTopic(Long id);
}
