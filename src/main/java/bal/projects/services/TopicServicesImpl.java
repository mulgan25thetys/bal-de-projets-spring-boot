package bal.projects.services;

import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import bal.projects.entities.Project;
import bal.projects.entities.Topic;
import bal.projects.repositories.TopicRepository;

@Service
public class TopicServicesImpl implements ITopicServices{

	@Autowired
	TopicRepository topicRepository;
	
	@Override
	public Page<Topic> findAll(Pageable pageable) {
		return topicRepository.findAllTopics(pageable);
	}

	@Override
	public Topic addTopic(Topic topic) {
		topic.setDateCreation(new Date());
		return topicRepository.save(topic);
	}

	@Override
	public Topic editTopic(Topic topic) {
		topic.setDateModification(new Date());
		return topicRepository.save(topic);
	}

	@Transactional
	public void removeTopic(Long id) {
		topicRepository.deleteById(id);
	}

	@Override
	public Topic getTopic(Long id) {
		return topicRepository.getById(id);
	}

	@Override
	public List<Project> getProjectsForByTopic(Long id) {
		Optional<Topic> topic = topicRepository.findById(id);
		
		if(topic.isPresent()) {
			return topic.get().getProjects();
		}
		return Collections.emptyList();
	}

	
}
