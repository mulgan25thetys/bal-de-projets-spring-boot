package bal.projects.services;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import bal.projects.entities.CriteriaEvaluation;
import bal.projects.repositories.CriteriaEvaluationRepository;

@Service
public class CriteriaEvaluationServicesImpl implements ICriteriaEvaluationServices{

	@Autowired
	CriteriaEvaluationRepository criteriaRepository;
	
	@Override
	public Page<CriteriaEvaluation> findAll(Pageable pageable) {
		return criteriaRepository.findAll(pageable);
	}

	@Override
	public CriteriaEvaluation addCriteriaEvaluation(CriteriaEvaluation criteria) {
		criteria.setDateCreation(new Date());
		return criteriaRepository.save(criteria);
	}

	@Override
	public CriteriaEvaluation getCriteriaEvaluation(Long id) {
		return criteriaRepository.findById(id).orElse(null);
	}

	@Override
	public CriteriaEvaluation editCriteriaEvaluation(CriteriaEvaluation criteria) {
		criteria.setDateModification(new Date());
		return criteriaRepository.save(criteria);
	}

	@Override
	public void removeCriteriaEvaluation(Long id) {
		Optional<CriteriaEvaluation> criteria = criteriaRepository.findById(id);
		
		if(criteria.isPresent()) {
			criteriaRepository.delete(criteria.get());
		}
	}

	@Override
	public List<CriteriaEvaluation> getAll() {
		
		return criteriaRepository.findAll();
	}
}
