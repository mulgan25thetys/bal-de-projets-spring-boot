package bal.projects.services;

import org.springframework.core.io.Resource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.multipart.MultipartFile;

import bal.projects.entities.Project;
import bal.projects.entities.Team;
import bal.projects.entities.Vote;

public interface IProjectServices {

	Page<Project> findAll(Pageable pageable);
	
	Project addProject(Long idTeam,Long idTopic,Project proj);
	
	Project saveFiles(MultipartFile file,Long idProj);
	
	Project editProject(Project proj);
	
	void removeProject(Long id);
	
	Project getProject(Long id);
	
	Project votesProject(Vote vote,Long idProject);
	
	Team getTeamByProject(Long id);
	
	Resource load(String filename);
	
}
