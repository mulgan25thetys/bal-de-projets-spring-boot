package bal.projects.services;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import bal.projects.entities.Agreement;
import bal.projects.entities.Team;

public interface ITeamServices {

	Team addTeam(Team team);
	
	Page<Team> findAll(Pageable pageable);
	
	Team editTeam(Team team);
	
	Team getTeam(Long id);
	
	Team setTutorsAgreements(Long idTutor,Long idTeam,Agreement agreement);
	
	Team addToProjectsBall(Long idTeam,Boolean affected);
	
	void removeTeam(Long id);
	
}
