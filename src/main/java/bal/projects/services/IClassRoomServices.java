package bal.projects.services;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import bal.projects.entities.ClassRoom;

public interface IClassRoomServices {

	ClassRoom addClass(ClassRoom classRoom);
	
	Page<ClassRoom> findAll(Pageable pageable);
	
	void removeClassRomm(Long id);
	
	ClassRoom editClassRoom(ClassRoom classRoom);
	
	ClassRoom getClassRoomById(Long id);
}
