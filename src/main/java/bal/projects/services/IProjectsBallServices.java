package bal.projects.services;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import bal.projects.entities.ProjectsBall;
import bal.projects.entities.Team;

public interface IProjectsBallServices {

	ProjectsBall addProjectBall(ProjectsBall pb);
	
	Page<ProjectsBall> findAll(Pageable pageable);
	
	ProjectsBall editProjectsBall(ProjectsBall pb);
	
	ProjectsBall getByid(Long id);
	
	List<Team> getAffectedTeams(Long id);
	
	Team findAffectedTeam(Long id,Long idTeam);
	
	void canceledProjectBall(Long id);
}
