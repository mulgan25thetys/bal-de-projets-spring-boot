package bal.projects.services;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import bal.projects.entities.ProjectsBall;
import bal.projects.entities.Team;
import bal.projects.repositories.ProjectsBallRepository;
import bal.projects.repositories.TeamRepository;

import java.util.Collections;

@Service
public class ProjectsBallServicesImpl implements IProjectsBallServices{

	@Autowired
	ProjectsBallRepository repoPB;
	
	@Autowired
	TeamRepository teamRepository;
	
	@Override
	public ProjectsBall addProjectBall(ProjectsBall pb) {
		pb.setEditionDate(new Date());
		return repoPB.save(pb);
	}

	@Override
	public Page<ProjectsBall> findAll(Pageable pageable) {
		return repoPB.findAll(pageable);
	}
	
	@Override
	public ProjectsBall editProjectsBall(ProjectsBall pb) {
		
		return repoPB.save(pb);
	}

	@Override
	public ProjectsBall getByid(Long id) {
		
		return repoPB.findById(id).orElse(null);
	}

	@Override
	public List<Team> getAffectedTeams(Long id) {
		ProjectsBall pb = repoPB.findById(id).orElse(null);
		if(pb != null) {
			return pb.getAffectedTeams();
		}
		return Collections.emptyList();
	}

	@Override
	public Team findAffectedTeam(Long id,Long idTeam) {
		ProjectsBall pb = repoPB.findById(id).orElse(null);
		if(pb != null) {
			Optional<Team> teamOptional = pb.getAffectedTeams().stream().filter(t -> t.getIdTeam().equals(idTeam)).findAny();
			if(teamOptional.isPresent()) {
				return teamOptional.get();
			}
		}
		return null;
	}

	@Override
	public void canceledProjectBall(Long id) {
		Optional<ProjectsBall> projectB = repoPB.findById(id);
		
		if(projectB.isPresent()) {
			ProjectsBall projectBall = projectB.get();
			for (Team team : projectBall.getAffectedTeams()) {
				team.setAffectedProjectBall(null);
				team.setIsAffected(false);
				teamRepository.save(team);
			}
			repoPB.delete(projectBall);
		}
	}

}
