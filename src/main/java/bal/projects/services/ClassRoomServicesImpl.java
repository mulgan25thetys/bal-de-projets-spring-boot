package bal.projects.services;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import bal.projects.entities.ClassRoom;
import bal.projects.repositories.ClassRoomRepository;

@Service
public class ClassRoomServicesImpl implements IClassRoomServices{

	@Autowired
	ClassRoomRepository classRepository;
	
	@Override
	public ClassRoom addClass(ClassRoom classRoom) {
		classRoom.setDateCreation(new Date());
		return classRepository.save(classRoom);
	}

	@Override
	public Page<ClassRoom> findAll(Pageable pageable) {
		
		return classRepository.findAll(pageable);
	}

	@Override
	public void removeClassRomm(Long id) {
		
		classRepository.deleteById(id);
	}

	@Override
	public ClassRoom editClassRoom(ClassRoom classRoom) {
		classRoom.setDateModification(new Date());
		return classRepository.save(classRoom);
	}

	@Override
	public ClassRoom getClassRoomById(Long id) {
		
		return classRepository.findById(id).orElse(null);
	}

}
