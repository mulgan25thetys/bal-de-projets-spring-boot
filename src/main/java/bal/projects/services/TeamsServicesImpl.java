package bal.projects.services;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import bal.projects.entities.Agreement;
import bal.projects.entities.ProjectsBall;
import bal.projects.entities.Team;
import bal.projects.entities.User;
import bal.projects.repositories.AgreementRepository;
import bal.projects.repositories.ProjectRepository;
import bal.projects.repositories.ProjectsBallRepository;
import bal.projects.repositories.TeamRepository;
import bal.projects.repositories.UserRepository;

@Service
public class TeamsServicesImpl implements ITeamServices{

	private static final Logger log = Logger.getLogger(TeamsServicesImpl.class);
	
	@Autowired
	TeamRepository teamRepository;
	
	@Autowired
	ProjectRepository projectRepository;
	
	@Autowired
	UserRepository userRepository;
	
	@Autowired
	AgreementRepository agreeRepository;
	
	@Autowired
	ProjectsBallRepository pBRepository;
	
	@Transactional
	public Team addTeam(Team team) {
		team.setDateCreation(new Date());
		Team addedTeam = teamRepository.save(team);
		if(!team.getIdsMembers().isEmpty()) {
			for (Long id : team.getIdsMembers()) {
				Optional<User> memberTeam = userRepository.findById(id);
				if(memberTeam.isPresent()) {
					memberTeam.get().setMyTeam(addedTeam);
					userRepository.save(memberTeam.get());
				}
			}
		}
		return addedTeam;
	}

	@Override
	public Page<Team> findAll(Pageable pageable) {
		return teamRepository.findAllTeams(pageable);
	}

	@Transactional
	public Team editTeam(Team team) {
		team.setDateModification(new Date());
		List<User> allMembers = userRepository.getMembersByTeam(team.getIdTeam());
		for (User user : allMembers) {
			if(!team.getIdsMembers().isEmpty()) {
				if(!team.getIdsMembers().contains(user.getId())) {
					user.setMyTeam(null);
					userRepository.save(user);
				}else {
					for (Long id : team.getIdsMembers()) {
						Optional<User> memberTeam = userRepository.findById(id);
						if(memberTeam.isPresent()) {
							memberTeam.get().setMyTeam(team);
							userRepository.save(memberTeam.get());
						}
					}
				}
			}
		}
		return teamRepository.save(team);
	}

	@Override
	public Team getTeam(Long id) {
		return teamRepository.findById(id).orElse(null);
	}

	@Transactional
	public Team setTutorsAgreements(Long idTutor,Long idTeam,Agreement agreement) {
		Optional<Team> team = teamRepository.findById(idTeam);
		Optional<User> tutor = userRepository.findById(idTutor);
		
		if(team.isPresent() && tutor.isPresent() && team.get().getTutors().getMembers().contains(tutor.get())) {
			agreeRepository.save(agreement);
			Team editedTeam = team.get();
			editedTeam.getAgreements().add(agreement);
			return teamRepository.save(editedTeam);
		}
		return null;
	} 

	@Transactional
	public Team addToProjectsBall(Long idTeam,Boolean affected) {
		Optional<ProjectsBall> pb = pBRepository.getProjectBallOfYear(Calendar.getInstance().get(Calendar.YEAR));
		Optional<Team> team = teamRepository.findById(idTeam);
		
		if(pb.isPresent() && team.isPresent()) {
			ProjectsBall projectB = pb.get();
			Team teamAf = team.get();
			
			if(Boolean.TRUE.equals(affected) ) {
				projectB.getAffectedTeams().add(teamAf);
				teamAf.setAffectedProjectBall(projectB);
			}else {
				projectB.getAffectedTeams().remove(projectB.getAffectedTeams().indexOf(teamAf));
				teamAf.setAffectedProjectBall(null);
			}
			
			teamAf.setIsAffected(affected);
			
			pBRepository.save(projectB);
			teamRepository.save(teamAf);
			
			return this.getTeam(idTeam);
		}
		return null;
	}

	@Scheduled(cron = "*/60 * * * * *" )
	public void participateToProjectBall() {
		Optional<ProjectsBall> pb = pBRepository.getProjectBallOfYear(Calendar.getInstance().get(Calendar.YEAR));
		List<Team> listTeams = teamRepository.findAllNoAffectedTeamsOfThisYear();
		
		if(pb.isPresent()) {
			ProjectsBall projectBall = pb.get();
			for (Team team : listTeams) {
				if(team.getMeanPI() >= 15 && team.getMeanPI() < 20) {
					projectBall.getAffectedTeams().add(team);
					team.setIsAffected(true);
					team.setAffectedProjectBall(projectBall);
					log.info("Ajout d'une equipe au Ball des projets: "+team.toString());
				}
			}
		}
	}

	@Override
	public void removeTeam(Long id) {
		Optional<Team> gottenTeam = teamRepository.findById(id);
		
		if(gottenTeam.isPresent()) {
			Team team = gottenTeam.get();
			for (User member : team.getMembers()) {
				member.setMyTeam(null);
				userRepository.save(member);
			}
			teamRepository.delete(team);
		}
	}

}
