package bal.projects.services;

import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Date;
import java.util.Optional;

import javax.transaction.Transactional;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import bal.projects.entities.Attachements;
import bal.projects.entities.Project;
import bal.projects.entities.Team;
import bal.projects.entities.Technology;
import bal.projects.entities.TechnologyLangage;
import bal.projects.entities.Topic;
import bal.projects.entities.Vote;
import bal.projects.repositories.AttachementsRepository;
import bal.projects.repositories.ProjectRepository;
import bal.projects.repositories.TeamRepository;
import bal.projects.repositories.TechnologyLangageRepository;
import bal.projects.repositories.TechnologyRepository;
import bal.projects.repositories.TopicRepository;
import bal.projects.repositories.VoteRepository;

@Service
public class ProjectServicesImpl implements IProjectServices{
	
	private final Path projectRoot = Paths.get("src/main/resources/uploads/projects");

	private static final Logger log = Logger.getLogger(ProjectServicesImpl.class);
	
	@Autowired
	TeamRepository teamRepository;
	
	@Autowired
	ProjectRepository projectRepo;
	
	@Autowired
	AttachementsRepository attachRepository;
	
	@Autowired
	TopicRepository topicRepository;
	
	@Autowired
	TechnologyRepository technoRepository;
	
	@Autowired
	TechnologyLangageRepository langageRepository;
	
	@Autowired
	VoteRepository voteRepository;
	
	@Override
	public Page<Project> findAll(Pageable pageable) {
		
		return projectRepo.findAll(pageable);
	}

	@Transactional
	public Project addProject(Long idTeam,Long idTopic, Project proj) {
		Optional<Topic> topic = topicRepository.findById(idTopic);
		Optional<Team> team = teamRepository.findById(idTeam);
		
		Project savingProject = new Project();
		
		if(team.isPresent() && topic.isPresent()) {
			Team teamToSave = team.get();
			Topic topicToSave = topic.get();
			
			if(proj.getTechnologies() != null) {
				for (Technology technology : proj.getTechnologies()) {
					for (TechnologyLangage langage : technology.getUsedLangages()) {
						langageRepository.save(langage);
					}
					technology.setDateCreation(new Date());
					technoRepository.save(technology);
				}
			}
			
			proj.setTeam(teamToSave);
			proj.setTopic(topicToSave);
			proj.setDateCreation(new Date());
			
			topicToSave.getProjects().add(proj);
			teamToSave.setProject(proj);
			
			savingProject = projectRepo.save(proj);
		}
		return savingProject;
	}
	
	@Transactional
	public Project saveFiles(MultipartFile file, Long idProj) {
		Optional<Project> project = projectRepo.findById(idProj);
		String filename = "";
		
		if(project.isPresent()) {
			Project projectToSave = project.get();
			Attachements projectFile = new Attachements();
			
			Optional<String> extension = Optional.ofNullable(file.getOriginalFilename()).filter(f -> f.contains("."))
				      .map(f -> f.substring(file.getOriginalFilename().lastIndexOf('.') + 1));
			if(extension.isPresent()) {
				
				projectFile.setExtension(extension.get());
				filename = projectToSave.getName()+new Date().getTime()+"."+extension.get();
			}
			
			try {
			   Files.copy(file.getInputStream(), this.projectRoot.resolve(filename));
			} catch (Exception e) {
				log.info("Could not store the file. Error: " + e.getMessage());   
			}
			projectFile.setName(filename);
			projectToSave.getFiles().add(projectFile);
			
			attachRepository.save(projectFile);
			projectRepo.save(projectToSave);
		}
		return null;
	}

	@Transactional
	public Project editProject(Project proj) {
		proj.setDateModification(new Date());
		if(proj.getTechnologies() != null) {
			for (Technology technology : proj.getTechnologies()) {
				for (TechnologyLangage langage : technology.getUsedLangages()) {
					langageRepository.save(langage);
				}
				technoRepository.save(technology);
			}
		}
		return projectRepo.save(proj);
	}

	@Override
	public void removeProject(Long id) {
		projectRepo.deleteById(id);
	}

	@Override
	public Project getProject(Long id) {
		return projectRepo.findById(id).orElse(null);
	}

	@Override
	public Project votesProject(Vote vote,Long idProject) {
		Optional<Project> project = projectRepo.findById(idProject);
		Project votedProject = new Project();
		
		if(project.isPresent()) {
			votedProject = project.get();
			vote.setProject(votedProject);
			
			votedProject.getVotes().add(voteRepository.save(vote));
			return projectRepo.save(votedProject);
		}
		return votedProject;
	}

	@Override
	public Team getTeamByProject(Long id) {
		Optional<Project> project = projectRepo.findById(id);
		if(project.isPresent()) {
			return project.get().getTeam();
		}
		return null;
	}
	
	@Override
	public Resource load(String filename) {
	    try {
	      Path file = projectRoot.resolve(filename);
	      Resource resource = new UrlResource(file.toUri());
	      if (resource.exists() || resource.isReadable()) {
	        return resource;
	      } else {
	    	  log.info("could not read file");
	    	  return null;
	      }
	    } catch (MalformedURLException e) {
	      log.debug(e);
	      return null;
	    }	    
	  }
}
