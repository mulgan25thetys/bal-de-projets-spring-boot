package bal.projects.services;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import bal.projects.entities.CriteriaEvaluation;

public interface ICriteriaEvaluationServices {

	Page<CriteriaEvaluation> findAll(Pageable pageable);
	
	List<CriteriaEvaluation> getAll();
	
	CriteriaEvaluation addCriteriaEvaluation(CriteriaEvaluation criteria);
	
	CriteriaEvaluation getCriteriaEvaluation(Long id);
	
	CriteriaEvaluation editCriteriaEvaluation(CriteriaEvaluation criteria);
	
	void removeCriteriaEvaluation(Long id);
}
