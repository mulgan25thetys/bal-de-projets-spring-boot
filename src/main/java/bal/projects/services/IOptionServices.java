package bal.projects.services;

import java.util.List;

import javax.validation.Valid;

import bal.projects.entities.Options;

public interface IOptionServices {

	List<Options> findAll();
	
	Options addOption(Options opt,Long idResponsable);
	
	Options editOption(Options opt);
	
	Boolean deleteOption(@Valid Long id);
	
	Options getOption(Long id);
}
