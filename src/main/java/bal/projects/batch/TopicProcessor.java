package bal.projects.batch;

import java.util.Date;

import org.springframework.batch.item.ItemProcessor;

import bal.projects.entities.Topic;

public class TopicProcessor implements ItemProcessor<Topic, Topic> {

    @Override
    public Topic process(Topic topic) {
    	topic.setTheme(topic.getTheme());
    	topic.setLevel(topic.getLevel());
    	topic.setDateCreation(new Date());
        return topic;
    }

}
