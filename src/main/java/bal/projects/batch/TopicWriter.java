package bal.projects.batch;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;

import bal.projects.entities.Topic;
import bal.projects.services.ITopicServices;

public class TopicWriter implements ItemWriter<Topic> {

	private static final Logger log = Logger.getLogger(TopicWriter.class);
	
    @Autowired
    private ITopicServices topicService;

    @Override
    public void write(List<? extends Topic> topics) {
        topics.stream().forEach(topic -> {
            log.info("Enregistrement en base de l'objet {}"+ topic);
            topicService.addTopic(topic);
        });
    }

}
