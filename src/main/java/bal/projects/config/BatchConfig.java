package bal.projects.config;


import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.LineMapper;
import org.springframework.batch.item.file.mapping.DefaultLineMapper;
import org.springframework.batch.item.file.transform.DelimitedLineTokenizer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.scheduling.annotation.EnableScheduling;

import bal.projects.batch.TopicProcessor;
import bal.projects.batch.TopicWriter;
import bal.projects.entities.Topic;
import bal.projects.entities.TopicFieldSetMapper;

@Configuration
@EnableBatchProcessing
@EnableScheduling
public class BatchConfig {

	private static final String FILE_NAME = "topics.csv";
    private static final String JOB_NAME = "listTopicsJob";
    private static final String STEP_NAME = "processingStep";
    private static final String READER_NAME = "topicItemReader";

    private String names = "theme,dateCreation";
    /*6. Définir la stratégie de délimitation des différents champs*/
    private String delimiter = ",";

    @Bean
    @Autowired
    public Step topicStep(StepBuilderFactory stepBuilderFactory) {
        return stepBuilderFactory.get(STEP_NAME)
                .<Topic, Topic>chunk(5)
                .reader(topicItemReader())
                .processor(topicItemProcessor())
                .writer(topicItemWriter())
                .build();
    }

    @Bean
    @Autowired
    public Job listTopicsJob(JobBuilderFactory jobBuilderFactory,Step step1) {
        return jobBuilderFactory.get(JOB_NAME)
                .start(step1)
                .build();
    }

    @Bean
    public ItemReader<Topic> topicItemReader() {
        FlatFileItemReader<Topic> reader = new FlatFileItemReader<>();
        reader.setResource(new ClassPathResource(FILE_NAME));
        reader.setName(READER_NAME);
        reader.setLinesToSkip(1);
        reader.setLineMapper(lineMapper());
        return reader;

    }

    @Bean
    public LineMapper<Topic> lineMapper() {

        final DefaultLineMapper<Topic> defaultLineMapper = new DefaultLineMapper<>();
        final DelimitedLineTokenizer lineTokenizer = new DelimitedLineTokenizer();
        lineTokenizer.setDelimiter(delimiter);
        lineTokenizer.setStrict(false);
        lineTokenizer.setNames(names.split(delimiter));

        final TopicFieldSetMapper fieldSetMapper = new TopicFieldSetMapper();
        defaultLineMapper.setLineTokenizer(lineTokenizer);
        defaultLineMapper.setFieldSetMapper(fieldSetMapper);

        return defaultLineMapper;
    }

    @Bean
    public ItemProcessor<Topic, Topic> topicItemProcessor() {
        return new TopicProcessor();
    }

    @Bean
    public ItemWriter<Topic> topicItemWriter() {
        return new TopicWriter();
    }
}
