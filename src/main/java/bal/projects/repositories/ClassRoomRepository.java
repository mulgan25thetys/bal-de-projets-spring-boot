package bal.projects.repositories;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import bal.projects.entities.ClassRoom;

@Repository
public interface ClassRoomRepository extends JpaRepository<ClassRoom, Long>{

	
	Boolean existsByBlocAndNumber(String bloc,int number);
	
	Optional<ClassRoom> findByBlocAndNumber(String bloc,int number);
}
