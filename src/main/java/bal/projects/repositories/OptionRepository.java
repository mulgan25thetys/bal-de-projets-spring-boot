package bal.projects.repositories;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import bal.projects.entities.Options;

@Repository
public interface OptionRepository extends JpaRepository<Options, Long>{

	@Query(value = "SELECT *FROM Options WHERE responsable_id =:id",nativeQuery = true)
	public Options getOptionForThisResponsable(@Param("id") Long idResponsable);
	
	Boolean existsByName(String name);
	
	Optional<Options> findByName(String name);
}
