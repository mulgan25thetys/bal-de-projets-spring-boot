package bal.projects.repositories;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import bal.projects.entities.User;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
	Optional<User> findByUsername(String username);
	
	@Query(value = "SELECT * FROM user WHERE email =:value OR username=:value",nativeQuery = true)
	User findByUsernameOrEmail(@Param("value") String value);
	
	@Query(value = "SELECT * FROM user WHERE my_option_id =:optionid",nativeQuery = true)
	List<User> getStudentsByOptionId(@Param("optionid") Long id);
	
	@Query(value = "SELECT * FROM user WHERE my_team_id is null",nativeQuery = true)
	List<User> getMembersForTeam();
		
	@Query(value = "SELECT * FROM user u INNER JOIN team t on u.my_team_id = t.id WHERE t.id =:teamid",nativeQuery = true)
	List<User> getMembersByTeam(@Param("teamid") Long id);
	
	Boolean existsByUsername(String username);
	Boolean existsByEmail(String email);

	boolean existsByIdentifiant(String identifiant);
}