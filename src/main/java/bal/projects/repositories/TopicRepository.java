package bal.projects.repositories;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import bal.projects.entities.Topic;

@Repository
public interface TopicRepository extends JpaRepository<Topic, Long>{

	Boolean existsByTheme(String theme);
	
	Optional<Topic> findByTheme(String theme);
	
	@Query(value = "SELECT * FROM topic ORDER BY level ASC",nativeQuery = true)
	Page<Topic> findAllTopics(Pageable pageable);
}
