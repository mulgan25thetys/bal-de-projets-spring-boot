package bal.projects.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import bal.projects.entities.Agreement;

@Repository
public interface AgreementRepository extends JpaRepository<Agreement, Long>{

}
