package bal.projects.repositories;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import bal.projects.entities.Team;

@Repository
public interface TeamRepository extends JpaRepository<Team, Long>{

	Boolean existsByName (String name);
	
	Optional<Team> findByName(String name);
	
	@Query(value = "SELECT * FROM team ORDER BY level ASC",nativeQuery = true)
	Page<Team> findAllTeams(Pageable pageable);
	
	@Query(value = "SELECT * FROM team t JOIN agreement a Where is_affected = 0",nativeQuery = true)
	List<Team> findAllNoAffectedTeamsOfThisYear();
	
	@Query(value = "SELECT * FROM team t INNER JOIN user u ON t.id = u.my_team_id WHERE u.id =:myId",nativeQuery = true)
	Team getMyTeam(@Param("myId") Long myId);
}
