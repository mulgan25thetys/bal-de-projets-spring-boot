package bal.projects.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import bal.projects.entities.TechnologyLangage;

@Repository
public interface TechnologyLangageRepository extends JpaRepository<TechnologyLangage, Long>{

}
