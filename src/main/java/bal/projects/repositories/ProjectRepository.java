package bal.projects.repositories;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import bal.projects.entities.Project;

@Repository
public interface ProjectRepository extends JpaRepository<Project, Long>{

	Boolean existsByName (String name);
	
	@Query(value = "Select *from project where team_id =:idTeam LIMIT 1",nativeQuery = true)
	Optional<Project> existsByTeam(@Param("idTeam") Long id);
	
	Optional<Project> findByName(String name);
}
