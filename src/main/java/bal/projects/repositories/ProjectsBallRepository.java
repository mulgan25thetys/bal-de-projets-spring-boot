package bal.projects.repositories;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import bal.projects.entities.ProjectsBall;


@Repository
public interface ProjectsBallRepository extends JpaRepository<ProjectsBall, Long>{

	Boolean existsByIdProjectsBall(Long id);
	
	Boolean existsByEdition(String edition);
	
	Boolean existsByTitle(String title);
	
	@Query(value = "SELECT * FROM projects_ball WHERE YEAR(edition_date) =:thisYear  LIMIT 1",nativeQuery = true)
	public Optional<ProjectsBall> getProjectBallOfYear(@Param("thisYear") int year);
	
}