package bal.projects.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import bal.projects.entities.Attachements;

@Repository
public interface AttachementsRepository extends JpaRepository<Attachements, Long>{

}
