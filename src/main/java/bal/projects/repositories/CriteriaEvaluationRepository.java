package bal.projects.repositories;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import bal.projects.entities.CriteriaEvaluation;

@Repository
public interface CriteriaEvaluationRepository extends JpaRepository<CriteriaEvaluation, Long> {

	Optional<CriteriaEvaluation> findByCriteria(String criteria);
	
	Boolean existsByCriteria(String criteria);
	
	Boolean existsByLevel(int level);
}
