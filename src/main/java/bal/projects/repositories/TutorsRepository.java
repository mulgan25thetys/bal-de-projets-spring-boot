package bal.projects.repositories;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import bal.projects.entities.Tutors;

@Repository
public interface TutorsRepository extends JpaRepository<Tutors, Long>{

}
