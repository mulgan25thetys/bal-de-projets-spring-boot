package bal.projects.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import bal.projects.entities.Technology;

@Repository
public interface TechnologyRepository extends JpaRepository<Technology, Long>{

}
