package bal.projects;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class BalProjetsApplication {

	public static void main(String[] args) {
		SpringApplication.run(BalProjetsApplication.class, args);
	}

}
