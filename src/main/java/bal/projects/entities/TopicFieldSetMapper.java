package bal.projects.entities;

import java.util.Date;

import org.springframework.batch.item.file.mapping.FieldSetMapper;
import org.springframework.batch.item.file.transform.FieldSet;

public class TopicFieldSetMapper  implements FieldSetMapper<Topic> {
    @Override
    public Topic mapFieldSet(FieldSet fieldSet) {
        return Topic.builder()
        	   .theme(fieldSet.readString(0))
        	   .level(Level.valueOf(fieldSet.readString(1)))
               .dateCreation(new Date())
               .build();
    } 

}
