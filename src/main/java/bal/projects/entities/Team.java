package bal.projects.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Entity
@Builder
@Getter
@Setter
@AllArgsConstructor
@ToString
public class Team implements Serializable{/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long idTeam;
	private String name;
	@Enumerated(EnumType.STRING)
	private Level level;
	private float meanPI;
	@Temporal(TemporalType.DATE)
	private Date dateCreation;
	@Temporal(TemporalType.DATE)
	private Date dateModification;
	private Boolean isAffected;
	
	@Transient
	private List<Long> idsMembers;
	
	@OneToMany(fetch = FetchType.EAGER,mappedBy = "myTeam",cascade = CascadeType.REFRESH)
	private List<User> members;
	
	@OneToMany(fetch = FetchType.LAZY)
	@JoinTable(	name = "team_agreements", joinColumns = @JoinColumn(name = "team_id"), 
	inverseJoinColumns = @JoinColumn(name = "agreement_id"))
	private List<Agreement> agreements;
	
	@OneToOne(mappedBy = "team",cascade = CascadeType.ALL)
	private Project project;
	
	@JsonIgnore
	@ManyToOne
	private ProjectsBall affectedProjectBall;
	
	@ManyToOne
	private ClassRoom affectedClassRoom;
	
	@ManyToOne
	private Tutors tutors;

	public Team() {
		super();
	}
	
	
}
