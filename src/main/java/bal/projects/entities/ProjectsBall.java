package bal.projects.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotBlank;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Entity
@Builder
@Getter
@Setter
@AllArgsConstructor
@ToString
public class ProjectsBall implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long idProjectsBall;
	@NotBlank
	private String edition;
	private String title;
	private String place;
	private Boolean cancelled;
	@Temporal(TemporalType.DATE)
	private Date editionDate;
	
	@OneToMany(mappedBy = "affectedProjectBall",cascade = CascadeType.REFRESH,fetch = FetchType.EAGER)
	private List<Team> affectedTeams;

	public ProjectsBall() {
		super();
	}
	
	
}
