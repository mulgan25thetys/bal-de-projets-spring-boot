package bal.projects.entities;

public enum Appreciation {
	EXCELLENT,VERY_GOOD,PASSABLE,MEAN,INSUFFICIENT
}
