package bal.projects.entities;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Entity
@Getter
@Setter
@ToString
@Builder
@AllArgsConstructor
public class Tutors implements Serializable{/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long idTutors;
	
	@ManyToMany(fetch = FetchType.LAZY,cascade = CascadeType.REFRESH)
	@JoinTable(	name = "tutors_group", joinColumns = @JoinColumn(name = "group_id"), 
		inverseJoinColumns = @JoinColumn(name = "user_id"))
	private List<User> members;

	@JsonIgnore
	@OneToMany(fetch = FetchType.LAZY,mappedBy = "tutors",cascade = CascadeType.REFRESH)
	private List<Team> teams;

	public Tutors() {
		super();
	}
	
	
}
